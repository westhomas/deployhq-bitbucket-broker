# DeployHQ Service Broker for Bitbucket.org


The custom service broker was created to enable deployment via deployhq.com's deployment service using your git or mercurial repository.

When invoked, it will parse a payload from the bitbucket service broker manager and then POST a request to the specified URL to invoke a deployhq deployment.

Contents:

- broker.py - the actual DeployHQBroker class
- brokers.py - the fake bitbucket brokers.BaseBroker class
- test.py - a few tests which parse bitbucket's payload data and then repost to the URL specified in paylod