import unittest
import json
from broker import DeployHQBroker

"""
Execute tests:
$ python tests.py


For debugging:
$ python -m pdb tests.py


Pre-reqs:
If you don't want the broker to fail to post to the service url, setup a POST
handler at http://localhost:8000/post_dump so you can see the output.


Ipython copy paste for interactive testing (enable alternate class statement):
In [1]: from tests import TestDeployHQ
In [2]: import json
In [3]: t = TestDeployHQ()
In [4]: t.setUp()
In [5]: j = json.loads(t.deployhq_test_post1)
In [6]: j2 = json.loads(t.deployhq_test_post2)
In [7]: j3 = json.loads(t.deployhq_test_post3)
"""
#class TestDeployHQ(): #Alternate class statement for interactive command line testing
class TestDeployHQ(unittest.TestCase):

    def setUp(self):
        self.deployhq_twitter = """
        {
            "broker": "twitter", 
            "commits": [
                {
                    "author": "jespern", 
                    "files": [
                        {
                            "file": "media/css/layout.css", 
                            "type": "modified"
                        }, 
                        {
                            "file": "apps/bb/views.py", 
                            "type": "modified"
                        }, 
                        {
                            "file": "templates/issues/issue.html", 
                            "type": "modified"
                        }
                    ], 
                    "message": "adding bump button, issue #206 fixed", 
                    "node": "e71c63bcc05e", 
                    "revision": 1650, 
                    "size": 684
                }
            ], 
            "repository": {
                "absolute_url": "/jespern/bitbucket/", 
                "name": "bitbucket", 
                "owner": "jespern", 
                "slug": "bitbucket", 
                "website": "http://bitbucket.org/"
            }, 
            "service": {"url": "http://localhost:8000/post_dump", "notification_email": "testemail@gmail.com" }
        }
        """

        #Test with all required params and optional branch params
        self.deployhq_test_post1 = """
        {
            "service": {"url": "http://localhost:8000/post_dump", "notification_email": "testemail@gmail.com", "deploy_branch": "master" },
            "canon_url": "https://bitbucket.org", 
            "commits": [
                {
                    "author": "testuser", 
                    "branch": "master", 
                    "files": [
                        {
                            "file": "webapp/settings.py", 
                            "type": "modified"
                        }
                    ], 
                    "message": "test commit 2\\n", 
                    "node": "cd4fbbdce7f4", 
                    "parents": [
                        "afa01ed6c229"
                    ], 
                    "raw_author": "Test User <testemail@gmail.com>", 
                    "raw_node": "cd4fbbdce7f4e95284d552ad5868acc1cf42252c", 
                    "revision": null, 
                    "size": -1, 
                    "timestamp": "2012-03-20 06:01:44", 
                    "utctimestamp": "2012-03-20 05:01:44+00:00"
                }
            ], 
            "repository": {
                "absolute_url": "/testuser/testrepo/", 
                "fork": false, 
                "is_private": true, 
                "name": "testrepo", 
                "owner": "testuser", 
                "scm": "git", 
                "slug": "testrepo", 
                "website": ""
            }, 
            "user": "testuser"
        }
        """

        # No branch param
        self.deployhq_test_post2 = """
        {
            "service": {"url": "http://localhost:8000/post_dump", "notification_email": "testemail@gmail.com" },
            "canon_url": "https://bitbucket.org", 
            "commits": [
                {
                    "author": "testuser", 
                    "branch": "master", 
                    "files": [
                        {
                            "file": "webapp/settings.py", 
                            "type": "modified"
                        }
                    ], 
                    "message": "test commit 2\\n", 
                    "node": "cd4fbbdce7f4", 
                    "parents": [
                        "afa01ed6c229"
                    ], 
                    "raw_author": "Test User <testemail@gmail.com>", 
                    "raw_node": "cd4fbbdce7f4e95284d552ad5868acc1cf42252c", 
                    "revision": null, 
                    "size": -1, 
                    "timestamp": "2012-03-20 06:01:44", 
                    "utctimestamp": "2012-03-20 05:01:44+00:00"
                },
                {
                    "author": "testuser", 
                    "branch": "master", 
                    "files": [
                        {
                            "file": "webapp/test.py", 
                            "type": "modified"
                        }
                    ], 
                    "message": "test commit 2\\n", 
                    "node": "123123123", 
                    "parents": [
                        "afa01ed6c229"
                    ], 
                    "raw_author": "Test User <testemail@gmail.com>", 
                    "raw_node": "123123123", 
                    "revision": null, 
                    "size": -1, 
                    "timestamp": "2012-03-21 06:01:44", 
                    "utctimestamp": "2012-03-21 05:01:44+00:00"
                }
            ], 
            "repository": {
                "absolute_url": "/testuser/testrepo/", 
                "fork": false, 
                "is_private": true, 
                "name": "testrepo", 
                "owner": "testuser", 
                "scm": "git", 
                "slug": "testrepo", 
                "website": ""
            }, 
            "user": "testuser"
        }
        """

        #Commits is a dict instead of list
        self.deployhq_test_post3 = """
        {
            "service": {"url": "http://localhost:8000/post_dump", "notification_email": "testemail@gmail.com", "deploy_branch": "master" },
            "canon_url": "https://bitbucket.org", 
            "commits": 
                {
                    "author": "testuser", 
                    "branch": "master", 
                    "files": [
                        {
                            "file": "webapp/settings.py", 
                            "type": "modified"
                        }
                    ], 
                    "message": "test commit 2\\n", 
                    "node": "cd4fbbdce7f4", 
                    "parents": [
                        "afa01ed6c229"
                    ], 
                    "raw_author": "Test User <testemail@gmail.com>", 
                    "raw_node": "cd4fbbdce7f4e95284d552ad5868acc1cf42252c", 
                    "revision": null, 
                    "size": -1, 
                    "timestamp": "2012-03-20 06:01:44", 
                    "utctimestamp": "2012-03-20 05:01:44+00:00"
                }
            , 
            "repository": {
                "absolute_url": "/testuser/testrepo/", 
                "fork": false, 
                "is_private": true, 
                "name": "testrepo", 
                "owner": "testuser", 
                "scm": "git", 
                "slug": "testrepo", 
                "website": ""
            }, 
            "user": "testuser"
        }
        """


    def test_posts(self):

        broker = DeployHQBroker()
        
        #broker.handle(json.loads(self.deployhq_actual))
        
        broker.handle(json.loads(self.deployhq_twitter))

        broker.handle(json.loads(self.deployhq_test_post1))
        
        broker.handle(json.loads(self.deployhq_test_post2))

        broker.handle(json.loads(self.deployhq_test_post3))

        self.assertTrue(True)

if __name__ == '__main__':
    unittest.main()